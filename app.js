var express = require('express');


var mongo = require('mongodb').MongoClient;
var monk = require('monk');
var db = monk('localhost:27017');
var assert = require('assert');
var app = express();



// file is included here:

var cors = require('cors');
var bodyParser = require("body-parser");
var app = express();

var server = require('http').createServer(app);
server.listen(4200);


app.use(cors());
// configure app to use bodyParser()

// this will let us get the data from a POST

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;        // set our port

// ROUTES FOR OUR API

// =============================================================================

var router = express.Router();              // get an instance of the express Router// middleware to use for all requests

router.use(function(req, res, next) {

    // do logging

    console.log('Something is happening.');

    next(); // make sure we go to the next routes and don't stop here

});// test route to make sure everything is working (accessed at GET http://localhost:8080/api)

router.get('/', function(req, res) {

    res.json({ message: 'hooray! welcome to our api!' });

});


router.get("/product/:barcode",function(req,res) {

    console.log('barcode:' + req.params.barcode);
    var product;
    mongo.connect("mongodb://localhost:27017/kProducts", function (err, db) {

        db.collection('kProducts', function (err, collection) {

            collection.findOne({'product.barcode': req.params.barcode}, function(err, result) {
                product = result;
                console.log("ok" +  result);
                res.json(product);
            });
      });


    });




});

app.use('/api', router);


app.listen(port, function() {
    console.log("application started");
});
